package com.itau.saqueinternacional;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class FileManager {

	//static Path pathAccount = Paths.get("/home/mastertech/workspace/aline/saqueinternacional/accounts.csv");
	//static Path pathWithdrawal = Paths.get("/home/mastertech/workspace/aline/saqueinternacional/withdrawals.csv");

	public static List<String> readFile(String path) {
		
		List<String> lines = new ArrayList<String>();
		
		try {
			lines = Files.readAllLines(Paths.get(path));
		} catch (IOException e) {
			System.out.println("Erro ao ler arquivo!");
			e.printStackTrace();
		}
		lines.remove(0);
		return lines;
	}
	
	public static boolean writeFile(String path, List<String> lines) {
		StringBuilder fileLines = new StringBuilder();
		
		for(String line : lines)
			fileLines.append(line + "\n");
		
		try {
			Files.write(Paths.get(path), fileLines.toString().getBytes());
			return true;
		} catch (IOException e) {
			System.out.println("Erro ao gravar arquivo!");
			e.printStackTrace();
		}
		return false;
	}
}
