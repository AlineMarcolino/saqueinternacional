package com.itau.saqueinternacional;

import java.util.ArrayList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	List<String> accounts = FileManager.readFile("/home/mastertech/workspace/aline/saqueinternacional/accounts.csv");
    	String[] accountFields;
    	List<Customer> customers = new ArrayList<Customer>();
    	
        for (String account: accounts) {
        	accountFields = account.split(",");
        	customers.add(new Customer(accountFields[0], accountFields[1], accountFields[2], Double.valueOf(accountFields[3]), AccountType.valueOf(accountFields[4])));
		}
        
        for (Customer customer : customers) {
			System.out.println(customer.toString());
		}
        
        List<String> withdrawal = FileManager.readFile("/home/mastertech/workspace/aline/saqueinternacional/withdrawals.csv");
    	String[] withdrawalFields;
    	List<Withdrawal> withdrawals = new ArrayList<Withdrawal>();
    	
        for (String w: withdrawal) {
        	withdrawalFields = w.split(",");
        	withdrawals.add(new Withdrawal(withdrawalFields[0], Double.valueOf(withdrawalFields[1]), withdrawalFields[2]));
		}
        
        for (Withdrawal with : withdrawals) {
			System.out.println(with.toString());
		}
    }
}
