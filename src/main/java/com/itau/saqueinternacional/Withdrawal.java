package com.itau.saqueinternacional;

public class Withdrawal {
	private String username;
	private double amount;
	private String currency;
	
	public Withdrawal(String username, double amount, String currency) {
		this.username = username;
		this.amount = amount;
		this.currency = currency;
	}

	@Override
	public String toString() {
		
		return String.format("%s %.2f %s", this.username, this.amount, this.currency);
	}
}
