package com.itau.saqueinternacional;

public class Customer {

	protected String name;
	protected String username;
	protected String password;
	protected double balance;
	protected AccountType type;
	
	public Customer(String name, String username, String password, double balance, AccountType type) {
		this.name = name;
		this.username = username;
		this.password = password;
		this.balance = balance;
		this.type = type;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return String.format("%s %s %s %.2f %s", this.name, this.username, this.password, this.balance, this.type);
	}

}